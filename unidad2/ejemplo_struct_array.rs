struct Cancion {
    titulo: String,
    artista: String,
    duracion: u32, // en segundos
}

struct ListaReproduccion {
    nombre: String,
    // Campo de tipo array
    canciones: [Cancion; 2],
}

fn main() {
    // Creamos una lista de reproducción
    let mut lista = ListaReproduccion {
        nombre: String::from("Mi Lista de Reproducción"),
        canciones: [
            Cancion {
                titulo: String::from("Canción 1"),
                artista: String::from("Artista 1"),
                duracion: 180,
            },
            Cancion {
                titulo: String::from("Canción 2"),
                artista: String::from("Artista 2"),
                duracion: 240,
            },
            // Puedes seguir agregando más canciones...
        ],
    };

    // Accedemos y mostramos información de la primera canción en la lista
    let primera_cancion = &lista.canciones[0];
    println!("Título: {}", primera_cancion.titulo);
    println!("Artista: {}", primera_cancion.artista);
    println!("Duración: {} segundos", primera_cancion.duracion);

    // Modificamos el nombre de la lista de reproducción
    lista.nombre = String::from("Nueva Lista de Reproducción");

    // Agregamos una nueva canción a la lista
    lista.canciones[1] = Cancion {
        titulo: String::from("Canción 3"),
        artista: String::from("Artista 3"),
        duracion: 210,
    };

    // Mostramos el nombre de la lista actualizado
    println!("Nombre de la Lista: {}", lista.nombre);
}

use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;


fn count_artist_occurrences(filename: &str){
    let file = open_file(&filename);
    let reader = io::BufReader::new(file);
    let mut artist_name = String::new();
    let mut max_artist_name = String::new();
    let mut counter = 0;
    let mut max_counter = 0;

    for line in reader.lines() {
        if let Ok(line) = line {
            let fields: Vec<&str> = line.split(';').collect();
            artist_name = fields[1].to_string();
            let file_to_compare = open_file(&filename);
            let reader_to_compare = io::BufReader::new(file_to_compare);
            counter = 0;
            for line_compare in reader_to_compare.lines(){
                if let Ok(line_compare) = line_compare {
                    let fields_to_compare: Vec<&str> = line_compare.split(';').collect();
                    let temp_artist_name = fields_to_compare[1].to_string();

                    if temp_artist_name == artist_name {
                        counter = counter + 1;
                    }
                }
            }

            if counter >= max_counter {
                max_counter = counter;
                max_artist_name = artist_name;
            }
        }
    }
    println!("El artista que tiene más canciones es: {max_artist_name} {max_counter}");
}


fn responses(file: File, field: usize, question: &str){

    let position_artist = 1;
    let position_song = 2;
    let position_to_find = field;
    let reader = io::BufReader::new(file);
    let mut max_streams = 0;
    let mut max_song_name = String::new();
    let mut max_artist_name = String::new();

    for line in reader.lines() {
        if let Ok(line) = line {
            // resulta más sencillo - permitir
            let fields: Vec<&str> = line.split(';').collect();
            // println!("{:?}", fields);
            // println!("{}", fields.len());

            if fields.len() <= 9 {
                let artist_name = fields[position_artist];
                let song_name = fields[position_song];
                let total_streams = fields[position_to_find].trim().parse::<i32>();

                if let Ok(streams) = total_streams {
                    if streams > max_streams {
                        max_streams = streams;
                        max_song_name = song_name.to_string();
                        max_artist_name = artist_name.to_string();
                    }
                } else {
                    println!("Error por lectura encabezado");
                }
            }
        }
    }

    println!("{question}: {max_song_name} de {max_artist_name}");

}

fn open_file(filename: &str) -> File {
    let archivo = Path::new(filename);
    let mut archivo = match File::open(&archivo) {
        Err(why) => panic!("{}",why),
        Ok(archivo) => archivo,
    };

    return archivo
}

fn main() {

    let filename = "Spotify_final_dataset.csv";
    //let archivo = Path::new("spotify.csv");
    let archivo = open_file(&filename);
    //pregunta 1
    let question = "La canción que más veces alcanzó la posición máxima es:  ";
    responses(archivo, 6, question);
    // pregunta 2
    let question = "La canción que más reproducciones ha tenido:  ";
    let archivo = open_file(&filename);
    responses(archivo, 8, question);
    // pregunta 3
    let question = "La canción que más reproducciones ha tenido cuando ha estado popular:  ";
    let archivo = open_file(&filename);
    responses(archivo, 7, question);
    // pregunta 4
    let question = "La canción que ha tenido el mayor numero de veces dentro del top 10:  ";
    let archivo = open_file(&filename);
    responses(archivo, 4, question);
    // pregunta 5
    let question = "La canción más antigua:  ";
    let archivo = open_file(&filename);
    responses(archivo, 3, question);

    //let filename = "spotify.csv";
    count_artist_occurrences(&filename);
}

struct Persona{
   nombre:String,
   apellido:String,
   edad:u32
}

fn eliminar_persona(mut persona: Persona) -> Persona{

    persona.nombre = "".to_string();
    persona.apellido = "".to_string();
    persona.edad = 0;

    return persona
}


fn editar_persona(mut persona: Persona) -> Persona{

    persona.nombre = "Juanito".to_string();
    persona.apellido = "Gomez".to_string();
    persona.edad = 55;

    return persona

}

fn listar_persona(persona: &Persona) -> (){
    println!("nombre: {} {} edad {}", persona.nombre,
             persona.apellido,
             persona.edad);

}

fn crea_persona() -> Persona{

    let nombre = String::from("Pepito");
    let apellido = String::from("Perez");
    let edad = 32;

    let nueva = Persona {
      nombre: nombre,
      apellido: apellido,
      edad: edad,
   };

   return nueva;
}


fn main(){

    let pepito = crea_persona();
    listar_persona(&pepito);

    let juanito = editar_persona(pepito);
    listar_persona(&juanito);

    let blanco = eliminar_persona(juanito);
    listar_persona(&blanco);

}

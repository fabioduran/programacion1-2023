use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

fn main() {
    let path = Path::new("hello.txt");
    let display = path.display();
    let mut file = match File::open(&path) {
        Err(why) => panic!("no se puede abrir {}: {}", display, why),
        Ok(file) => file,
    };


    // Leer contenido del archivo
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("couldn't read {}: {}",
                           display,
                           why),
        Ok(_) => print!("{} contiene:\n{}",
                        display,
                        s),
    }

}

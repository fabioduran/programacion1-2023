use std::fs::File;
use std::io::{BufRead, BufReader, Error};

fn leer_archivo_linea_por_linea(nombre_archivo: &str) -> Result<(), Error> {
    // Abre el archivo en modo lectura
    let file = File::open(nombre_archivo)?;

    // Crea un buffer con el contenido
    let reader = BufReader::new(file);

    // recorre el archivo linea a línea
    for line in reader.lines() {
        match line {
            Ok(text) => {
                println!("{}", text);
            }
            Err(err) => {
                eprintln!("Error al leer una línea: {}", err);
            }
        }
    }

    return Ok(())
}

fn main() {
    if let Err(err) = leer_archivo_linea_por_linea("tabla_periodica.txt") {
        println!("Error al leer el archivo: {}", err);
    }
    else {
        println!("Fin");

    }
}

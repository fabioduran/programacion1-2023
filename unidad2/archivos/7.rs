use std::fs::OpenOptions;
use std::io::prelude::*;

fn main() {
  let mut file = OpenOptions::new()
    .write(true)
    .append(true)
    .create(true)
    .open("foo.txt")
    .unwrap();
  if let Err(e) = writeln!(file, "Nueva") {
    println!("No se puede escribir: {}", e);
  }
}

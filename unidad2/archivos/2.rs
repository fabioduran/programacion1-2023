static TEXTO: &str = "Hola Mundo";
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

fn main() {
    let path = Path::new("nuevo_archivo.txt");
    let display = path.display();

    // al abrir devuelve io::Result<File>
    let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why),
        Ok(file) => file,
    };

    match file.write_all(TEXTO.as_bytes()) {
        Err(why) => panic!("No se puede crear {}: {}",
                           display,
                           why),
        Ok(_) => println!("Se ha creado el archivo {}",
                          display),
    }

}

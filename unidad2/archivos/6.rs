use std::io::prelude::*;
use std::io::BufWriter;
use std::fs::File;

// importante que siempre devuelve un tipo de datos
fn main() -> std::io::Result<()> {
    let mut buffer = BufWriter::new(File::create("foo.txt")?);

    buffer.write_all(b"Texto lo que sea ")?;
    buffer.write_all("Hola Mundo".as_bytes())?;
    buffer.flush()?;
    Ok(())
}

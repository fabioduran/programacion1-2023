use std::fs;

fn main() {

    let filename = "tabla_periodica.txt";
    println!("Archivo {}", filename);

    let contents = fs::read_to_string(filename)
        .expect("Error de lectura del archivo");
    println!("El contenido del archivo es:\n{}", contents);
}

struct Persona {
    nombre: String,
    edad: u32,
    ciudad: String,
}


fn obtener_descripcion(dato: Persona) -> String {
    let datos = format!("{} de {} años, de la ciudad de {}", dato.nombre, dato.edad, dato.ciudad);
    return datos
}


fn main() {
    // Crear una instancia de Persona
    let persona1 = Persona {
        nombre: String::from("Juan"),
        edad: 30,
        ciudad: String::from("Ciudad A"),
    };

    let texto = obtener_descripcion(persona1);
    println!("{texto}");

}

fn main(){
    let mut arreglo: [u32; 5] = [0; 5];

    arreglo[1] = 100000;

    println!("{:?}", arreglo);

    for indice in arreglo{
        println!("{indice} ");

    }

    for indice in 1..arreglo.len(){
        println!("{} ", arreglo[indice]);
    }

    arreglo[3] = 100;
    let mut indice = 0;
    println!("Usando loop");
    loop{
        println!("{} ", arreglo[indice]);
        indice = indice + 1;
        if indice == arreglo.len(){
            break;
        }

    }

    println!("iter");
    for indice in arreglo.iter(){
        println!("{indice}");
    }

    let mut my_keywords:[&str; 2] = ["Hola Mundo"; 2];

    for my_keyword in my_keywords.iter() {
        for key in my_keyword.chars(){
            println!("{key}");
        }
        println!("{}", my_keyword);
    }


  let matriz:[[i32;2];3] = [[8;2];3];
  println!("{:?}", matriz);
    for (i, row) in matriz.iter().enumerate() {
        println!("{:?}", row);
    for (j, col) in row.iter().enumerate() {
        println!("[row={}][col={}]={}", i, j, col);
    }
  }
    //accede al valor de forma directa
    println!("{}", matriz[0][0]);











}

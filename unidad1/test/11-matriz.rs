fn main(){
    let matriz:[[i32;2];3] = [[8;2];3];
    println!("{:?}", matriz);
    for (i, row) in matriz.iter().enumerate() {
        for (j, col) in row.iter().enumerate() {
            println!("[row={}][col={}]={}", i, j, col);
        }
    }
    //accede al valor de forma directa
    println!("{}", matriz[0][0]);
}

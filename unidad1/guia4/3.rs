use std::io;

fn texto_numero() -> i32 {
    loop {
        println!("Ingrese un número: ");
        let mut numero = String::new();
        let stdin = io::stdin();
        stdin.read_line(&mut numero).unwrap();
        let numero: i32 = match numero.trim().parse(){
            Ok(numero) => numero,
            Err(_) => {
                println!("Error, no es un número");
                continue;
            },
        };
        return numero;
    }
}


fn buscar_numero(arreglo: &[i32], valor_encontrar: i32) -> bool {
    for &numero in arreglo {
        if numero == valor_encontrar {
            return true;
        }
    }
    // valor no encontrado
    return false
}

fn main() {
    let arreglo = [5, 32, 42, 65, 23, 55, 33, 100, 11, 1, 6];

    let valor_buscar = texto_numero();
    let resultado = buscar_numero(&arreglo, valor_buscar);

    if resultado {
        println!("El número {} está presente en el arreglo.", valor_buscar);
    } else {
        println!("El número {} no está presente en el arreglo.", valor_buscar);
    }
}

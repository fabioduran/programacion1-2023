fn cuenta_pares(arreglo: &[i32]) -> usize {
    let mut contador = 0;

    for &numero in arreglo {
        if numero % 2 == 0 {
            contador = contador + 1;
        }
    }
    return contador
}


fn main() {
    let arreglo = [5, 32, 42, 65, 23, 55, 33, 100, 11, 1, 6];
    let pares = cuenta_pares(&arreglo);

    println!("En el arreglo hay {pares} números pares");

}

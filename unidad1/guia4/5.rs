fn elimina_duplicados(arreglo: &[i32; 10]) -> [i32; 10]{

    // creo nuevo arreglo para insertar solo valores únicos
    // se deja a cero como valor neutro o por defecto.
    let mut nuevo_arreglo: [i32; 10] = [0; 10];

    for x in 0..arreglo.len() -1{
        for y in 1..arreglo.len(){

            if arreglo[x] == arreglo[y]{
                nuevo_arreglo[y] = arreglo[y];
                break;
            }
        }
    }

    return nuevo_arreglo
}


fn main(){

    let arreglo = [1, 1, 2, 2, 3, 3, 4, 4, 1, 1];
    let arreglo = elimina_duplicados(&arreglo);
    println!("{:?}", arreglo);
}

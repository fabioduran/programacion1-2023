use std::io;

fn texto_numero() -> i32 {
    loop {
        println!("Ingrese un número: ");
        let mut numero = String::new();
        let stdin = io::stdin();
        stdin.read_line(&mut numero).unwrap();
        let numero: i32 = match numero.trim().parse(){
            Ok(numero) => numero,
            Err(_) => {
                println!("Error, no es un número");
                continue;
            },
        };
        return numero;
    }
}

fn crea_arreglo_numeros() -> [i32; 10]{

    let mut arreglo: [i32; 10] = [0; 10];
    let mut largo;

    loop {
    println!("Ingrese la cantidad de elementos a ingresar, debe ser menor a 100:");
    largo = texto_numero();

        if largo <= 10 {
            break
        } else {
            println!("Debe ingresar un numero entre 0 y 10");
        };
    };

    for indice in 0..largo as usize{
        let ingreso = texto_numero();
        arreglo[indice] = ingreso;
    }

    println!("{:?}", arreglo);
    return arreglo

}

fn calcula_promedio_arreglo(arreglo: &[i32]) -> f32 {
    let mut suma = 0;

    for numero in arreglo {
        suma = suma + numero;
    }

    let suma = suma as f32;
    let largo = arreglo.len() as f32;
    let promedio = suma / largo;

    return promedio
}


fn main() {
    //let numeros = [1, 2, 3, 4, 5];

    let arreglo = crea_arreglo_numeros();
    let resultado =  calcula_promedio_arreglo(&arreglo);

    println!("El arreglo tiene los siguientes datos {:?}", arreglo);
    println!("El promedio de los elementos del arreglo es: {}", resultado);
}

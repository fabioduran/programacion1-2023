use std::io;

fn texto_numero() -> i32 {
    loop {
        println!("Ingrese un número: ");
        let mut numero = String::new();
        let stdin = io::stdin();
        stdin.read_line(&mut numero).unwrap();
        let numero: i32 = match numero.trim().parse(){
            Ok(numero) => numero,
            Err(_) => {
                println!("Error, no es un número");
                continue;
            },
        };
        return numero;
    }
}


fn minimo_maximo(arreglo: [i32; 10]) -> (){

    let mut minimo: i32 = 0;
    let mut maximo: i32 = 0;

    for numero in arreglo {
        if minimo == 0{
            minimo = numero;
        }
        else if numero < minimo {

            minimo = numero;
        }

        if numero > maximo{
            maximo = numero;
        }
    }

    println!("El valor mínimo es {minimo}");
    println!("El valor máximo es {maximo}");

}


fn crea_arreglo() -> [i32; 10]{

    let mut arreglo: [i32; 10] = [0; 10];


    for indice in 0..10 as usize{
        let ingreso = texto_numero();
        arreglo[indice] = ingreso;
    }

    return arreglo

}

fn main(){

    let arreglo = crea_arreglo();
    minimo_maximo(arreglo);



}

fn ordenar_arreglo(mut arreglo: [i32; 10]) -> [i32; 10] {
    let n = arreglo.len();

    for i in 0..n {
        for j in i + 1..n {
            if arreglo[i] > arreglo[j] {
                // fuerza bruta, compara elemento a elemento
                let temp = arreglo[i];
                arreglo[i] = arreglo[j];
                arreglo[j] = temp;
            }
        }
    }

    return arreglo
}

fn main() {
    let arreglo = [3, 7, 4, 1, 8, 9, 2, 6, 5, 3];

    let nuevo = ordenar_arreglo(arreglo);

    println!("Arreglo original: {:?}", arreglo);
    println!("Arreglo ordenado: {:?}", nuevo);
}

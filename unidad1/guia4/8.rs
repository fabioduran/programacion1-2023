fn fibonacci(numero: u32) -> u32 {
    if numero <= 0 {
        return 0;
    } else if numero == 1 {
        return 1;
    }

    let mut a = 0;
    let mut b = 1;
    let mut resultado = 0;

    for num in 2..=numero {
        resultado = a + b;
        a = b;
        b = resultado;
    }

    return resultado
}

fn crea_arreglo() -> [u32; 10]{

    let mut arreglo: [u32; 10] = [0; 10];


    for indice in 0..10 as usize{
        arreglo[indice] = fibonacci(indice as u32);
    }

    return arreglo

}

fn main(){

    let arreglo = crea_arreglo();

    println!("{:?}", arreglo);


}

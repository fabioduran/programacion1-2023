fn suma_matrices(m1: [[i32; 5]; 5],
                 m2: [[i32; 5]; 5]) -> [[i32; 5]; 5] {

    let mut matriz_suma: [[i32; 5]; 5] = [[0; 5]; 5];
    let largo_x = m1[0].len();


    for x in 0..largo_x as usize {
        for y in 0..largo_x as usize {
            matriz_suma[x][y] = m1[x][y] + m2[x][y];
        }

    }

    return matriz_suma
}


fn main(){

    // matrices con valor 1
    let matrix1: [[i32; 5]; 5] = [[1; 5];5];
    let matrix2: [[i32; 5]; 5] = [[1; 5];5];

    let matriz_suma = suma_matrices(matrix1,
                                    matrix2);

    println!("matriz 1 {:?}", matrix1);
    println!("matriz 2 {:?}", matrix1);
    println!("la suma de las matrices es {:?}", matriz_suma);



}

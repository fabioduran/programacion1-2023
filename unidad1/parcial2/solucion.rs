fn contar_patron_arreglo(arreglo: &[&str], patron: &str) -> i32 {
    let mut contador = 0;

    for palabra in arreglo {
        // siempre recorrerá desde el largo de la palabra menos el patron
        // en este caso el patron el largo - 3
        // el + 1 corresponde a que el ciclo for recorre hasta el máximo -1
        let largo = palabra.len() - patron.len() + 1;
        for indice in 0..largo {
            // aplicar slice o rebanado al texto desde la posición
            // actual del indice hasta indice + el largo del patron
            // que para el caso es + 3
            let subcadena = &palabra[indice..indice + patron.len()];
            println!("{subcadena}");

            // compara siempre los mismo largo
            if subcadena == patron {
                contador += 1;
            }
        }
    }

    return contador
}

fn main() {
    let arreglo_strings = ["manzana", "banana", "pera", "uva", "manzana verde", "sandia"];

    let patron_busqueda = "ana";

    let cantidad_encontrada = contar_patron_arreglo(&arreglo_strings,
                                                    &patron_busqueda);

    println!("El patrón '{}' se encontró {} veces en el arreglo.", patron_busqueda, cantidad_encontrada);
}

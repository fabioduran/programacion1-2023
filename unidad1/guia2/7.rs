fn devuelve_primer_caracter(cadena: &str) -> char{

    let mut caracter: char = 'a';
    for letra in cadena.chars(){
        caracter = letra;
        break;
    }

    return caracter;

}

// fn devuelve_primer_caracter_v2(cadena: &str) -> char{

//     return cadena.chars().next();

// }

fn main(){
    let cadena = String::from("Esto es una cadena de texto");

    let primer_caracter = devuelve_primer_caracter(&cadena);
    println!("{primer_caracter}");

}

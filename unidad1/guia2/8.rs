fn devuelve_ultimo_caracter(cadena: &str) -> char{

    let mut ultimo_caracter = 'a';
    for letra in cadena.chars(){
        ultimo_caracter = letra;
    }

    return ultimo_caracter;
}

fn main(){

    let cadena = String::from("Esto es una cadena de texto");

    let ultimo_caracter = devuelve_ultimo_caracter(&cadena);
    println!("{ultimo_caracter}");


}

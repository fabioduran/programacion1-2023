fn letras_no_contenidas(palabra1: &str, palabra2: &str) -> String {
    let mut letras_faltantes = String::new();

    for letra1 in palabra1.chars() {
        let mut encontrada = false;

        for letra2 in palabra2.chars() {
            if letra1 == letra2 {
                encontrada = true;
                break;
            }
        }

        if !encontrada {
            letras_faltantes.push(letra1);
        }
    }

    println!("{letras_faltantes}");
}

fn main() {
    let palabra1 = "hola";
    let palabra2 = "chao";

    let letras_faltantes = letras_no_contenidas(palabra1, palabra2);

    println!("Letras no contenidas en la segunda palabra: {}", letras_faltantes);
}

fn elimina_vocales(texto: String) -> (){

    let mut resultado = String::new();
    // convierte texto a minuscula
    let texto = texto.to_lowercase();
    for caracter in texto.chars(){

        if caracter != 'a' &&
           caracter != 'e' &&
           caracter != 'i' &&
           caracter != 'o' &&
           caracter != 'u' {
               resultado.push(caracter);
        }
    }
    println!("El texto sin vocales es: {resultado}");
}

fn main(){

    let cadena_entrada = String::from("Hola Mundo");
    elimina_vocales(cadena_entrada);

}

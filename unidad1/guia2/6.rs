use std::io;

fn determina_mayor(numero1: u16, numero2: u16)-> u16{

    if numero1 > numero2{
        return numero1;
    }
    return numero2;

}

fn main(){

    let stdin = io::stdin();

    println!("Ingrese un número (1):");
    let mut numero1 = String::new();
    stdin.read_line(&mut numero1).unwrap();
    let numero1: u16 = numero1.trim().parse().unwrap();

    println!("Ingrese un número (2):");
    let mut numero2 = String::new();
    stdin.read_line(&mut numero2).unwrap();
    let numero2: u16 = numero2.trim().parse().unwrap();

    if numero1 == numero2{

        println!("{numero1} y {numero2} son iguales");
    } else {
        let numero_mayor = determina_mayor(numero1, numero2);
        println!("{numero_mayor} es el número mayor");
    }

}

use std::io;

fn convertir_a_fahrenheit(temperatura_celsius: f32) -> f32 {
    // Celsius a Fahrenheit: F = (C * 9/5) + 32
    let temperatura_fahrenheit = (temperatura_celsius * 9.0/5.0) + 32.0;
    return temperatura_fahrenheit
}

fn main() {
    println!("Ingrese la temperatura en Celsius:");

    let mut entrada = String::new();
    let stdin = io::stdin();
    stdin.read_line(&mut entrada).unwrap();

    let temperatura_celsius: f32 = entrada.trim().parse().unwrap();

    let temperatura_fahrenheit = convertir_a_fahrenheit(temperatura_celsius);

    println!("La temperatura en Fahrenheit es: {:.2}", temperatura_fahrenheit);
}

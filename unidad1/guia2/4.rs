use std::io;

fn evaluar_menor(numero_menor: i16, numero_actual: i16)-> i16{

    if numero_actual < numero_menor{
        return numero_actual
    } else {
        return numero_menor
    }

}


fn evaluar_mayor(numero_mayor: i16, numero_actual: i16)-> i16{

    if numero_actual > numero_mayor{
        return numero_actual
    } else {
        return numero_mayor
    }

}


fn main(){

    let stdin = io::stdin();
    let mut cantidad_numeros = String::new();
    let mut mayor = 0;
    let mut menor = 0;

    println!("Ingrese la cantidad de números a evaluar");
    stdin.read_line(&mut cantidad_numeros).unwrap();
    let cantidad_numeros: i16 = cantidad_numeros.trim().parse().unwrap();

    for _i in 0..cantidad_numeros{
        let mut numero_ingresado = String::new();
        let stdin = io::stdin();
        stdin.read_line(&mut numero_ingresado).unwrap();
        let numero_ingresado: i16 = numero_ingresado.trim().parse().unwrap();

        if menor == 0{
            menor = numero_ingresado;
        } else {
            menor = evaluar_menor(menor, numero_ingresado);
        }

        mayor = evaluar_mayor(mayor, numero_ingresado);
    }

    println!("El número menor ingresado es {menor} y el mayor es {mayor}");




}

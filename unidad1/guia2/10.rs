fn bisiesto(anio: u32) -> bool{

    if anio % 4 == 0 && anio % 100 != 0 || anio % 400 == 0{
        return true
    } else {
        return false
    }

}

fn main(){

    let anio = 2001;
    if bisiesto(anio) {
        println!("Bisiesto");
    } else {
        println!("NO BISIESTO");
    }

}

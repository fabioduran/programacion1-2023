fn cuenta_vocales(cadena: &str) ->(){

    let cadena = cadena.to_lowercase();
    let mut contador = 0;
    for caracter in cadena.chars(){
        if caracter == 'a' ||
            caracter == 'e' ||
            caracter == 'i' ||
            caracter == 'o' ||
            caracter == 'u' {
                contador = contador + 1;
            }
    }

    println!("La cantidad de vocales en {cadena} es {contador}");



}

fn main(){

    let cadena = String::from("Esto es una cadena de texto");
    cuenta_vocales(&cadena);

}

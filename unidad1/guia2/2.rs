fn encontrar_letra(texto: &str, letra: char) -> i16{

    let mut contador: i16 = 0;
    for caracter in texto.chars(){
        println!("{caracter}");
        if caracter == letra{
            contador = contador + 1
        }
    }

    return contador

}


fn main() {

    // Es una cadena de texto (flujo)
    let cadena = String::from("Esto es una cadena de texto");
    let letra = 'e';
    //pasamos el valor de cadena, pero el dueño de valor es main
    //Esto se entiende porque luego de pasar los valores a la función
    //El valor de la variable cadena es nuevamente utilizado por la función
    // main, y como se vuelve a utilizar se usa & (&cadena)
    let coincide = encontrar_letra(&cadena, letra);
    println!("El número de {letra} encontrado en {cadena} es: {coincide}");

}

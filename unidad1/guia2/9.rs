use std::io;

fn reloj_futuro(hora_actual: i32, hora_adicional: i32) -> (){

    let hora_futuro = (hora_actual + hora_adicional) % 12;

    println!("En {} horas, el reloj marcará las {}", hora_adicional, hora_futuro);

}

fn main() {
    println!("Hora actual:");
    let mut hora_actual = String::new();
    let stdin = io::stdin();
    stdin.read_line(&mut hora_actual).unwrap();

    let hora_actual: i32 = hora_actual.trim().parse().unwrap();

    println!("Cantidad de horas:");
    let mut horas_adicionales = String::new();
    let stdin = io::stdin();
    stdin.read_line(&mut horas_adicionales).unwrap();

    let horas_adicionales: i32 = horas_adicionales.trim().parse().unwrap();

    reloj_futuro(hora_actual, horas_adicionales);

}

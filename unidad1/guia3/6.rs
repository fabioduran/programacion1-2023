use std::io;

fn contiene_solo_letras(texto: &str) -> bool {
    // evalúa carácter a carácter
    for letra in texto.chars() {
        match letra {
            // patron de letras en minuscula y mayuscula
            'a'..='z' | 'A'..='Z' => continue,
            // _ significa para todo el resto
            _ => return false,
        }
    }
    return true
}

fn main() {

    let stdin = io::stdin();
    println!("Ingrese un texto: ");
    let mut entrada = String::new();
    stdin.read_line(&mut entrada).unwrap();

    //limpio la cadena
    let entrada = entrada.trim();
    let es_valido = contiene_solo_letras(&entrada);

    if es_valido {
        println!("La cadena contiene solo letras.");
    } else {
        println!("La cadena contiene caracteres no válidos.");
    }
}

use std::io;

fn calcular_potencia(base: u32, exponente: u32) -> u32 {

    // recursividad
    if exponente == 0 {
        return 1
    } else {
        return base * calcular_potencia(base, exponente - 1);
    }

}

fn main(){

    let stdin = io::stdin();

    // reutilización mismo nombre de variable (base)
    let base = loop{
        println!("Ingrese una base");
        let mut base = String::new();
        stdin.read_line(&mut base).unwrap();
        let base: u32 = match base.trim().parse() {
            Ok(base) => base,
            Err(_) => {
                println!("Error");
                continue;
            },
        };
        break base;
    };

    // Crea nuevas variables para asignar valores (exponente y numero_exponente)
    let numero_exponente = loop {
        println!("Ingrese un exponente");
        let mut exponente = String::new();
        stdin.read_line(&mut exponente).unwrap();
        let numero_exponente: u32 = match exponente.trim().parse(){
            Ok(exponente) => exponente,
            Err(_) => continue,
        };
        break numero_exponente;
    };

    let resultado = calcular_potencia(base, numero_exponente);
    println!("El resultado de {base} elevado a {numero_exponente} es {resultado}");
}

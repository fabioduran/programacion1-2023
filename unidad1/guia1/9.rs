use std::io::stdin;

fn main(){

    let entrada = stdin();
    let mut multiplicacion = 1;
    let mut cantidad_numero = String::new();

    println!("Escriba la cantidad de numeros a multiplicar");
    entrada.read_line(&mut cantidad_numero).unwrap();
    let cantidad_numero: i16 = cantidad_numero.trim().parse().unwrap();

    for _indice in 0..cantidad_numero{
        let mut numero = String::new();
        println!("Ingrese un número: ");
        entrada.read_line(&mut numero).unwrap();
        let numero: i16 = numero.trim().parse().unwrap();
        multiplicacion = multiplicacion * numero;
    }

    println!("La multiplicación de los números ingresados es: {multiplicacion}");

}

use std::io::stdin;

fn main(){

    let entrada = stdin();
    let mut numero = String::new();
    println!("Ingrese un numero: ");
    entrada.read_line(&mut numero).unwrap();
    let numero: i16 = numero.trim().parse().unwrap();

    // factorial
    println!("El factorial de {numero} es:");
    for indice in 0..numero{
        println!("{}", (numero * indice));

    }
}

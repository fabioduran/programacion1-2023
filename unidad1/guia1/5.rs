fn main(){

    let mut contador = 0;

    for indice in contador..10{
        println!("{indice}");
    }

    //opción 2, usando while

    println!("Usando while");
    while contador <=9 {
        println!("{contador}");
        contador = contador + 1;

    }

    //opción 3, usando loop
    println!("Usando loop");
    contador = 0;
    loop{
        if contador == 10{
            break;
        } else {
            println!("{contador}");
            contador = contador + 1;
        }

    }

}

use std::io;

fn main() {
    let mut edad = String::new();
    println!("¿Qué edad tienes?");
    let stdin = io::stdin();
    stdin.read_line(&mut edad).unwrap();

    let edad: u8 = edad.trim().parse().unwrap();

    if edad >= 18 {
        println!("Puedes votar");
    } else {
        println!("No tienes la edad para votar");
    }
}

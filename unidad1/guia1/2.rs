use std::io;

fn main(){

    let mut nombre = String::new();
    let stdin = io::stdin();

    println!("Ingrese su nombre: ");
    stdin.read_line(&mut nombre).unwrap();
    println!("El nombre ingresado es: {}", nombre);
}

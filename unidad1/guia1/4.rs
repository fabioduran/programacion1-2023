use std::io;

fn main(){

    let mut numero = String::new();
    let stdin = io::stdin();

    println!("Ingrese un número");
    stdin.read_line(&mut numero).unwrap();

    let numero: u16 = numero.trim().parse().unwrap();

    if numero % 2 == 0 {
        println!("El número es par")
    } else {
        println!("El número es impar")
    }

}

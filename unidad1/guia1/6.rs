fn main(){

    let mut contador = 11;

    for indice in (0..contador).rev(){
        println!("{indice}");

    }

    println!("Usando while");
    contador = 10;
    while contador >= 0 {
        println!("{contador}");
        contador = contador - 1;

    }

    println!("Usando loop");
    contador = 10;
    loop{
        if contador >= 0{
            break
        } else {
            println!("{contador}");
            contador = contador - 1;
        }
    }

}
